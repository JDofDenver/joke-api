import requests
import json

def joke():
    start = input("Want to hear a joke? Y/N \n")
    if start.upper() == "Y":
        category = ""
        select = input("Pick a Category: \n1 = Any, \n2 = Programming, \n3 = Dark, \n4 = Pun, \n5 = Spooky\n")
        if select == "1":
            category = "Any"
        elif select == "2":
            category = "Programming"
        elif select == "3":
            category = "Dark"
        elif select == "4":
            category = "Pun"
        elif select == "5":
            category = "Spooky"
            # print("\n")
        incoming = requests.get(url=f"https://v2.jokeapi.dev/joke/{category}")
        joke = json.loads(incoming.text)
        print(joke["setup"])
        print("\n")
        punchline = input("Press P for the punchline:\n")
        if punchline.upper() == "P":
            print(joke["delivery"])

joke()
